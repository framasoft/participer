[![](https://framasoft.org/nav/img/icons/ati/sites/git.png)](https://framagit.org)

🇬🇧 **Framasoft uses GitLab** for the development of its free softwares. Our Github repositories are only mirrors.
If you want to work with us, **fork us on [framagit.org](https://framagit.org)**. (no registration needed, you can sign in with your Github account)

🇫🇷 **Framasoft utilise GitLab** pour le développement de ses logiciels libres. Nos dépôts Github ne sont que des miroirs.
Si vous souhaitez travailler avec nous, **forkez-nous sur [framagit.org](https://framagit.org)**. (l’inscription n’est pas nécessaire, vous pouvez vous connecter avec votre compte Github)
* * *

Site de documentation et d’accueil (basée sur Honkit) des contributeurs aux projets de Framasoft.
[participer.framasoft.org](https://participer.framasoft.org)

* * *

## Contribuer à la documentation

### Rédaction

Les fichiers sont rédigés avec [la syntaxe markdown](https://docs.framasoft.org/fr/mattermost/help/messaging/formatting-text.html)
mais Honkit accepte aussi la syntaxe HTML.

Par contre, il n’est pas possible de mixer les 2 syntaxes dans un même bloc.

Si vous voulez ajouter des liens internes, il faut faire attention à pointer vers
les pages `.html` correspondantes au `.md` du dossier.

### Méthode de travail

https://framagit.org/spf/Tutos/blob/master/Git/translate.md
(voir si le tuto est adapaté pour la documentation)

  1. Forker
  2. Créer une branche
  3. Faire les changements (commit)
  4. Demander à intégrer les changements (merge request)
  5. Discuter / rectifier

### Structure

Les documentations anglaise et française sont totalement indépendantes.

La documentation de chaque Framaprojet doit être enregistrée dans un dossier.

Dans ce dossier, créer un dossier `images` contenant les captures d’écran
et un fichier `README.md` qui contiendra la page d’accueil de présentation du projet

Pour ajouter ensuite le Framaprojet dans la colonne de gauche et sur la page d’accueil,
il faut éditer les fichiers `SUMMARY.md` et `README.md` (à la racine dans le dossier `fr`).

Dans `SUMMARY.md`, il n’est pas possible d’afficher une arborescence
de plus de 2 niveaux et les titres ne sont pas pris en compte
(ils ne sont laissés qu’à titre indicatif).

Chaque lien `.md` trouvé dans ce fichier sera pris en compte par Honkit
pour créer le fichier `.html` correspondant.
Les fichiers qui ne s’y trouvent pas seront placé dans le dossier
comme de simples fichiers statiques (au même titre que les images)

## Créer une copie locale de la documentation

Pour avoir un aperçu du rendu du site il peut être souhaitable d’installer Honkit localement.

### Installation de NodeJS

Installez NodeJS et NPM :

    sudo apt-get install nodejs-legacy npm

### Générer la documentation

Clonez votre fork de la documentation

    git clone https://framagit.org/<utilisateur>/framasoft/participer.git

Depuis le dossier `participer` procédez à l’installation de honkit et des plugins requis
(la liste des plugins et leur configuration se trouve dans le fichiers `book.json`)

    cd participer
    npm run commons
    npm run dev

Si tout s’est bien déroulé (la commande se termine par `Serving book on…`,
la copie du site peut être consultée sur :

    http://localhost:4000/

Plus il y a de pages, plus le temps de chargement est long.

Il vaut mieux prendre le temps de finir les changements et redémarrer
le serveur manuellement quant on a fini.

Si vous disposez d’un serveur web en local, plutôt que de se servir du serveur
nodejs, vous pouvez aussi créer le site manuellement avec la commande

    npm run prod

et copier le dossier `_book` où vous le voulez sur le serveur web.