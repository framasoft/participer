#!/bin/bash

# Install packages
npm i

# Fix gitbook-plugin-edit-link (for Gitlab wiki)
sed -i "s/var filepath = gitbook.state.filepath;/var filepath = gitbook.state.filepath.replace(\/.md$\/, '\/edit');/" node_modules/gitbook-plugin-edit-link/book/plugin.js

# Clean book
if [ -d en ]; then
  find en -depth -maxdepth 1 -mindepth 1 -not -name 'book.json' -not -name '_layouts' -not -name 'styles' -exec rm -rf "{}" \;
fi

if [ -d fr ]; then
  find fr -depth -maxdepth 1 -mindepth 1 -not -name 'book.json' -not -name '_layouts' -not -name 'styles' -exec rm -rf "{}" \;
fi

# Import wiki
WikiBase=$(cat book.json | grep -Po '"base": "\K[^"]*');
WikiGit=$(echo $WikiBase | rev | cut -d '/' -f4- | rev).wiki.git;

git clone $WikiGit tmp/

## Copy uploads & fix paths in .md
if [ -d tmp/uploads ]; then
  cp -r ./tmp/uploads ./uploads
  if [ "$CI_JOB_NAME" = "pages" ]; then
    find tmp -iname '*.md' -exec sed -i "s/\](uploads/\](\.\.\/\.\.\/\.\.\/$(echo $CI_PAGES_URL | cut -d/ -f4-)\/uploads/g" "{}" \;
  else
    find tmp -iname '*.md' -exec sed -i "s/\](uploads/\](\.\.\/\.\.\/\.\.\/uploads/g" "{}" \;
  fi
fi

## Copy .md
if [ -d tmp/en ]; then
  mv tmp/en/* en/
fi

if [ -d tmp/fr ]; then
  mv tmp/fr/* fr/
fi

rm -rf tmp/

# Honkit
npx honkit init
